let i = 1;
let nbrePhotos = 8;
function diapo(numero, myId) {
    img = document.getElementById(myId);
    i = numero;
    if (i > nbrePhotos) { i = 1;} //au-delà de la dernière photo, revient à la première
    if (i < 1) { i = nbrePhotos ;} //en deçà de la première photo, affiche la dernière
    img.src = i + '.png';
    switch (i) {
        case 1 :
        case 2 :
            nom = "Hugues Capet";
            break;
        case 3 :
        case 4 :
            nom = "Philippe Auguste";
            break;
        case 5 :
        case 6 :
            nom = "Louis IX";
            break;
        case 7 :
        case 8 :
            nom = "Philippe IV";
            break;
    }
    document.getElementById("nomDuRoi").innerHTML = '<div>' + nom + '</div>';
}

function diapo2(numero, myId) {
    img = document.getElementById(myId);
    j = numero;
    img.src = j + '.png';
    switch (j) {
        case 1 :
        case 2 :
            nom = "Hugues Capet";
            break;
        case 3 :
        case 4 :
            nom = "Philippe Auguste";
            break;
        case 5 :
        case 6 :
            nom = "Louis IX";
            break;
        case 7 :
        case 8 :
            nom = "Philippe IV";
            break;
    }
    document.getElementById("nomDuRoi2").innerHTML = '<div>' + nom + '</div>';
}

function diapoAuto() {
    let j = 1;
    setInterval(function(){
        if (j > 8) { j = 1;}
        diapo2(j++, "diapo2")}
    , 2000);
}

function horloge(){
    setInterval(function() {
    heure = new Date();
    if (heure.getMinutes()%2) { //minutes impaires
        document.getElementById("clock").innerHTML = '<div style="color: blue;">' + heure.toLocaleTimeString() + '</div>';
    } else {
        document.getElementById("clock").innerHTML = '<div style="color: green;">' + heure.toLocaleTimeString() + '</div>';
    }} , 1000)
}

function activeRubrique(element) {
    let rubriques = document.getElementsByClassName('rubrique');
    for (let r = 0; r < rubriques.length; r++) {
        rubriques[r].style.borderBottom = 'none';
    }
    element.style.borderBottom = '2px solid #15376a';
}